# Password Cracking # 

### challenge one ###
* 1. what is the md5 hash of this pass? (calicuz!0290) -> 416ca0b0aa4f38a17da2853df2ba9451

* 2. what is the sha1 hash of this pass? (08tzuculetz) -> 7ba178a9561993a4441364535ac2dda3a4ff86f7

* 3. what is the sha256 hash of this pass? (28tify314) -> a91129fc2d025772c383580e1ba9f5d70b6166d5ca53546c4302274c6fd1540a

### challenge two ###

1. Our officers have obtained password dumps storing hacker passwords. After obtaining a few plaintext passwords, it appears that they overlap with the passwords from the rockyou breach.

	for this I downloaded the rockyou.txt wordlist. 
	...then I created a file with all the hashes given. 
	... then ran hashcat to print the results into a resulting txt file.  
	"hashcat -a 0 -m 0 -w 4 --status -o ./results.txt ./hashes.txt ./rockyou.txt"

this gives -> b834a64bf1e6b785280e4872d6ecf2ec:barndogg7
	      3c54aca99a6f12fac75adfe1bb794f08:phoebecat5
	      753a7675bc14b1693cce3bd1efe89c3e:applefish412
