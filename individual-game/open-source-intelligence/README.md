
# Open Source Intelligence # 

###  first challenge ###
*  comprised of using the whois command from terminal, forgot to record this before moving on

### second challenge ###
*  What is the US EIN of the company operating as apple.com -> 942404110

* What is the US EIN of the company operating as usps.com -> 41-0760000

* What is the US EIN of the company operating as spectrum.com? -> 841496755

* A tax paying adult, Tom, has the TIN of 578-10-****, what city was Tom born in? -> District of Columbia

* What is Facebook's Company Number for their operations in the United Kingdom? -> 06331310 (after some reading, I found that company numbers are found on tax filings so I went to the companies house uk website and searched for facebook and then dug thorugh their tax filings to find the number)

### third challenge ###
* "caucus  report" -> Following the disastrous failure of the 2020 Iowa Caucus, you are tasked with investigating the organizations responsible for providing the voting app used during the caucus. Use online resources to conduct research on the company that made the app (The Company) and one of their affiliates/investors, ACRONYM.

* 1.What is the name of The Company that developed the 2020 Iowa Caucus app? -> Shadow Inc

* 2. What is The Company's website? -> shadowinc.io

* 3. Instead of using the Google Play Store or Apple App Store, what distribution platform did The Company use to distribute the Iowa Caucus app? -> TestFairy 

 
