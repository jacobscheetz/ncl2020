# Password Cracking #

### Challenge: Cracking (Medium) ###
*  Our officers have obtained password dumps storing hacker passwords. It appears that they are all in the format: "SKY-TYFY-" followed by 4 digits. Can you crack them?

* you can solve this by using the masking attack in hashcat by generating all possible combinations and then running it against the parameter
	
	
