# Open Source Intelligence # 

### Challenge: Code(Hard) ###

* given a picture image- by binwalking the image we can see that there is a zip file included in the image, this gives us 29.txt which is empty and 29.zlib
* by using an online zlib viewer Ifound out that the file is a binary so I stuck it into ghidra
* 






### Challenge: Standard Numbers (Medium) ###

* Conduct research on the standard identifying numbers for the book, Hacking Exposed Wireless, 3rd Edition from McGraw-Hill (Paperback).

* Question: What is the ISBN-10 number for the book? -> 0071827638

* Question: What is the ISBN-13 number for the book (no dashes)? -> 9780071827638 

* Question: What is the ASIN number? -> 0071827633
